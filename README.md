# plasy.info

Plasy.info

## Planned / possible blocks

- Pollution + temperature
- Weather forecast
- Calendar: culture, sport, where/when to sport?
- Menus (Rudolf, Primary school, Kino, others?)
- Official gazette
- news from plasy.cz
- Contracts
- invoices
- Opinions
- Transport - traffic (number of cars), timetables?
- Advertisement (see e.g. mobile Aktualne)
- Contacts, links (Fb group)

## Other features
- sharing
- infinite scrolling (news?)
- content user control (switch on/off)
- feedback
